import React, { useEffect, useRef } from 'react'

const Messages = ({ messages, currentUser }) => {
    const chatContainer = useRef();
    useEffect(() => {
        scrollIntoBottom();
    }, [messages]);
    const scrollIntoBottom = () => {
        const scroll = chatContainer.current.scrollHeight - chatContainer.current.clientHeight;
        chatContainer.current.scrollTo(0, scroll);
    }

    let renderMessage = (message) => {
        const { sender, content, color } = message;
        const messageFromMe = currentUser.username === message.sender;
        const className = messageFromMe ? "Messages-message currentUser" : "Messages-message";
        return (
            <li className={className}>
                <span
                    className="avatar"
                    style={{ backgroundColor: color }}
                />
                <div className="Message-content">
                    <div className="username">
                        {sender}
                    </div>
                    <div className="text">{content}</div>
                </div>
            </li>
        );
    };


    return (

        <ul className="messages-list" ref={chatContainer}>
            {messages.map(msg => renderMessage(msg))}
        </ul>
    )
}


export default Messages