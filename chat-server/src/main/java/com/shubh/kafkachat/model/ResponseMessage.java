package com.shubh.kafkachat.model;

public class ResponseMessage {
    private int checkExist;

    public ResponseMessage(int checkExist) {
        this.checkExist = checkExist;
    }

    public ResponseMessage() {
    }

    public int getCheckExist() {
        return checkExist;
    }

    public void setCheckExist(int checkExist) {
        this.checkExist = checkExist;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
